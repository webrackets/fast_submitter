fast_submitter
==============

Little piece of code to submit forms fast (just by pressing `ENTER` key - not only when You are in textbox but everywhere else)

Just copy `fast_submitter.js` to any Rails project `app\assets\javascripts` folder and add `enterable_form` class to form which You want to submit fast.

Don't forget to include this file to `application.js` list:
    `//= require fast_submitter`


#Example:
    = form_for @book, :html => {:class => 'enterable_form'} do |f|
      %p= f.text_field :title
      %p= f.text_field :author
      = f.submit 'save'

Enjoy fast submitting forms!
If You have any suggestions - feel free to commit or just write to me directly!
