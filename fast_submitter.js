(function ($) {
  $(function () {
    //$('input[type=submit]').hide();     //uncomment this if You want to hide the submit form buttons
    document.onkeypress = function (event) {
      if (event.keyCode === 13) {
        $('.enterable_form').submit();
      }
    };
  });
}(jQuery));
